#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"
#include "Display.h"

Display::Display()
{
	screenWidth = 800;
	screenHeight = 600;
	screenBPP = 32;
	Init();
}

Display::~Display()
{
	SDL_Quit();
}

void Display::Init()
{
	//SDL
	SDL_Init( SDL_INIT_EVERYTHING );
	//ResizeWindow( screenWidth, screenHeight, false );
	SDL_SetVideoMode( screenWidth, screenHeight, screenBPP, SDL_OPENGL | SDL_RESIZABLE );
	SDL_WM_SetCaption( "ET GL", NULL );

	//Init
	glEnable( GL_DEPTH_TEST );
	glDepthFunc( GL_LEQUAL );
	
	//Resize
	ResizeWindow( screenWidth, screenHeight, false );
}

void Display::ResizeWindow( int width, int height, bool fullscreen )
{
	float ratio;
	if ( height == 0 )
		height = 1;

	ratio = (float)width / (float)height;
	glViewport( 0, 0, width, height );

	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();

	gluPerspective( 45.0f, ratio, 0.1f, 200);

	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
}

void Display::SetWindowCaption( char* caption )
{
	SDL_WM_SetCaption( caption, NULL );
}

void Display::Refresh()
{
	SDL_GL_SwapBuffers();
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

void Display::Draw( MapGrid map[], Player player, Camera camera )
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glLoadIdentity();
	glPushMatrix();
	glRotatef( camera.Angle(), 1, 0, 0 );
	glScalef( 0.2, 0.2, 0.2 );
	glTranslatef( camera.CameraX(), camera.CameraY(), camera.CameraZ() );
	
	int index = camera.CurrentMap();

	map[ index ].Draw( 0, 0 );

	map [ map[ index ].adjacentUp ].Draw( 0, 50 );
	map [ map[ index ].adjacentDown ].Draw( 0, -50 );
	map [ map[ index ].adjacentLeft ].Draw( -75, 0 );
	map [ map[ index ].adjacentRight ].Draw( 75, 0 );

	map [ map [ map[index].adjacentLeft ].adjacentDown ].Draw( -75, -50 );
	map [ map [ map[index].adjacentLeft ].adjacentUp ].Draw( -75, 50 );
	map [ map [ map[index].adjacentRight ].adjacentDown ].Draw( 75, -50 );
	map [ map [ map[index].adjacentRight ].adjacentUp ].Draw( 75, 50 );

	map [ map [ map[index].adjacentLeft ].adjacentLeft].Draw( -75, 100 );
	map [ map [ map[index].adjacentRight ].adjacentRight ].Draw( 75, 100 );
	map [ map [ map[index].adjacentUp ].adjacentUp ].Draw( 0, 100 );

	player.Draw();

	glPopMatrix();
	SDL_GL_SwapBuffers();
}
