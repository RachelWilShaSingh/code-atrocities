#pragma once
#include "Side.h"

/*
An object class to make creating and displaying
the world's objects "easier".
*/

enum ObjectType { TREE, BED, CHAIR, TABLE, YARD, HOUSE };

class RachObject
{
    private:
        float cX, cY, cZ;       //centerpoint X, Y, and Z coordinates
        Side *side;             //an object has an array of sides
        int sideAmt;
        float scale, rotation;
    public:        
        void Setup( int sideAmt );       
        void SetupObject( ObjectType type ); 
        void DrawObject();
        void SetCoordinates( float x, float y, float z );
        void Rotation( float rotation ) { this->rotation = rotation; }
        void Scale( float scale ) { this->scale = scale; }
        float Rotation() { return rotation; }
        float Scale() { return scale; }
        RachObject();        
        RachObject( int sideAmt );        
        ~RachObject();
};
