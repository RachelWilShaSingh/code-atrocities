#
# Rachel J. Morris learns Assembly
# 01 - Input and Output
# 
#   Cross Reference:
# 

		.data
Intro:	.asciiz		"\nWelcome to the program!"
NewLn:	.asciiz		"\n"
End:	.asciiz		"\nEnd of program!"
GetNum:	.asciiz		"\nPlease enter a number"
GetStr:	.asciiz		"\nPlease enter a string"
ResNum:	.asciiz		"\nNumber entered was: "
ResStr:	.asciiz		"\nString entered was: "

		.align 2
strIn:	.word		# a variable in memory; will hold string
		.align 2
intIn:	.word		# a variable in memory; will hold integer		

		.globl		main
	
		.text

main:

		##########  INTRODUCTION  ##########
		# display "Welcome to the program!"
		li $v0, 4			# load syscall; address of string (set up for string output)
		la $a0, Intro		# load address of string into parameter $a0
		syscall				# display
	
		##########  PROMPT USER FOR INFORMATION  ##########
	
		# display "Please enter a number"
		li $v0, 4			# prepare to output string
		la $a0, GetNum		# load GetNum as parameter
		syscall				# display

		# get number from user - page 31
		li $v0, 5			# prepare to receive number, stored in $v0
		syscall				# get	
		move $t0, $v0		# store the keyboard buffer into $t0.

		# store number as a variable
		sw $t0, intIn		# intIn = $t0

		# display "Please enter a string"
		li $v0, 4			# prepare to output string
		la $a0, GetStr		# load GetStr as parameter
		syscall				# display	

		# get string from user
		li $a1, 8			# load 8 as a parameter (length of string)
		li $v0, 8			# prepare to receive string
		la $a0, strIn		# load address of input buffer - must be a variable (not register)
		syscall				# get	

		##########  OUTPUT THE INFORMATION THAT WAS OBTAINED  ##########

		# display "Number entered was: "
		li $v0, 4			# prepare to output string
		la $a0, ResNum		# load ResNum as parameter
		syscall				# display

		# display intIn
		li $v0, 1			# prepare to output integer
		move $a0, $t0		
		syscall				# display

		# display the number from the variable
		li $v0, 1
		move $a0, intIn
		syscall

		# display New line
		li $v0, 4			# prepare to output string
		la $a0, NewLn		# load NewLn as parameter
		syscall				# display

		# display "String entered was: "
		li $v0, 4			# prepare to output integer
		la $a0, ResStr		# load ResStr as parameter
		syscall				# display

		# display strIn
		li $v0, 4			# prepare to output string
		la $a0, strIn		# load strIn as parameter
		syscall				# display

		##########  END OF PROGRAM, SAY GOODBYE THEN QUIT  ##########
		# display "End of program!"
		li $v0, 4			# set up for string output
		la $a0, End			# load address of string into parameter
		syscall				# display

		# End the program
		li $v0, 10			# set up for ending program
		syscall				# end
