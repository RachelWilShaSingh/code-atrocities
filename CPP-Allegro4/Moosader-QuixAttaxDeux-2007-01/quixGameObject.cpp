#include <allegro.h>
#include "quixGameObject.h"

bool quixGameObject::getEnabled() { return enabled; }
void quixGameObject::setEnabled(bool newval) { enabled = newval; }

void quixGameObject::changeScreenSize(int type) {
  if (type == 0) { set_gfx_mode(GFX_AUTODETECT_WINDOWED, 800, 600, 0, 0); }
  else { set_gfx_mode(GFX_AUTODETECT, 800, 600, 0, 0); }
}
END_OF_FUNCTION(changeScreenSize);


bool MenuObject::isDone() {
  return done;
}
END_OF_FUNCTION(isDone);

int MenuObject::returnControlScheme(int index) {
  return controlScheme[index];
}

void MenuObject::setControlScheme(int index, int schemenumber) {
  /*
  0 - WASD        1 - IJKL
  2 - ARROWS      3 - NUMPAD
  4 - JOY1        5 - JOY2
  6 - JOY3        7 - JOY4
  */

  controlScheme[index] = schemenumber;
}

void MenuObject::quit() {
  done = true;
}
END_OF_FUNCTION(quit);

MenuObject::MenuObject() {
  bmpBackground = load_bitmap("quiximgBackgroundMenu.bmp", NULL);
  bmpWindow = load_bitmap("quiximgWindow.bmp", NULL);
  bmpExit = load_bitmap("quiximgExitButton.bmp", NULL);
  bmpExitDepress = load_bitmap("quiximgExitButtonB.bmp", NULL);
  bmpPlay = load_bitmap("quiximgPlayButton.bmp", NULL);
  bmpPlayDepress = load_bitmap("quiximgPlayButtonB.bmp", NULL);
  bmpCursor = load_bitmap("quiximgCursor.bmp", NULL);
  bmpControlScheme = load_bitmap("quiximgKeySchemes.bmp",NULL);
  bmpArrows = load_bitmap("quiximgArrows.bmp",NULL);
  bmpScreenOpts = load_bitmap("quiximgWindowOpts.bmp",NULL);
  mouseDown = false;
  ExitPressed = false;
  PlayPressed = false;
  resetPressed = false;
  done = false;
  fragLimitHit = false;
  xPlay = 265;
  yPlay = 440;
  xExit = 465;
  yExit = 440;
  controlScheme[0] = 0;
  controlScheme[1] = 1;
  controlScheme[2] = 2;
  controlScheme[3] = 3;
  fragLimit = 0;
  setEnabled(true);
  barrierScheme = 0;
  setFullscreen(true);
}

bool quixGameObject::getFullscreen() { return fullscreen; }
void quixGameObject::setFullscreen(bool newval) { fullscreen = newval; }

int MenuObject::CheckClick(int X, int Y) {
  if (  (X >= xPlay) && (X <= xPlay+69) &&
        (Y >= yPlay) && (Y <= yPlay+50) ) {
          return 0;
        }
  if (  (X >= xExit) && (X <= xExit+69) &&
        (Y >= yExit) && (Y <= yExit+50) ) {
          return 1;
        }

  /*----------------------------------------*/
  //Player 1's Left Arrow
  if (  (X >= 155) && (X <= 155+15) &&
        (Y >= 225) && (Y <= 225+30) ) {
          return 2;
        }
  //Player 1's Right Arrow
  if (  (X >= 233) && (X <= 233+15) &&
        (Y >= 225) && (Y <= 225+30) ) {
          return 3;
        }

  //Player 2's Left Arrow
  if (  (X >= 280) && (X <= 280+15) &&
        (Y >= 225) && (Y <= 225+30) ) {
          return 4;
        }
  //Player 2's Right Arrow
  if (  (X >= 358) && (X <= 358+15) &&
        (Y >= 225) && (Y <= 225+30) ) {
          return 5;
        }

  //Player 3's Left Arrow
  if (  (X >= 430) && (X <= 430+15) &&
        (Y >= 225) && (Y <= 225+30) ) {
          return 6;
        }
  //Player 3's Right Arrow
  if (  (X >= 508) && (X <= 508+15) &&
        (Y >= 225) && (Y <= 225+30) ) {
          return 7;
        }

    //Player 4's Left Arrow
  if (  (X >= 555) && (X <= 555+15) &&
        (Y >= 225) && (Y <= 225+30) ) {
          return 8;
        }
  //Player 4's Right Arrow
  if (  (X >= 630) && (X <= 630+15) &&
        (Y >= 225) && (Y <= 225+30) ) {
          return 9;
        }

  if (  (X >= 166) && (X <= 166+15) &&
        (Y >= 340) && (Y <= 340+30) ) {
          return 14;
        }

  if (  (X >= 300) && (X <= 300+15) &&
        (Y >= 340) && (Y <= 340+30) ) {
          return 15;
        }

  if (  (X >= 166) && (X <= 166+15) &&
        (Y >= 370) && (Y <= 370+30) ) {
          return 16;
        }
  if (  (X >= 300) && (X <= 300+15) &&
        (Y >= 370) && (Y <= 370+30) ) {
          return 17;
        }

  //Fullscreen Button
  if (  (X >= 350) && (X <= 350+90) &&
        (Y >= 340) && (Y <= 340+30) ) {
          return 18;
  }

  //Reset Button
  if (  (X >= 350) && (X <= 350+90) &&
        (Y >= 371) && (Y <= 371+30) ) {
          return 19;
  }


  return 10;
}

LevelObject::LevelObject() {
  iFragLimit = 0;
  bmpBackground = load_bitmap("quiximgBackgroundGrass.bmp",NULL);
  bmpPlayerFilmstrip = load_bitmap("quiximgPlayerSet.bmp",NULL);
  bmpLaserFilmstrip = load_bitmap("quiximgLasers.bmp",NULL);
  frame = 0;
  iBarrierScheme = 0;
  setBarrierAmount(0);
}

int LevelObject::getFragLimit() { return iFragLimit; }
void LevelObject::setFragLimit(int value) { iFragLimit = value; }

int LevelObject::getBarrierScheme() { return iBarrierScheme; }
void LevelObject::setBarrierScheme(int value) { iBarrierScheme = value; }

void LevelObject::setFrame(int value) { frame = value; }
int LevelObject::getFrame() { return frame; }

int LevelObject::getBarrierX(int index) { return barrierX[index]; }
void LevelObject::setBarrierCoordinates(int index, int valueX, int  valueY) {
  barrierX[index] = valueX;
  barrierY[index] = valueY;
}

int LevelObject::getBarrierY(int index) { return barrierY[index]; }

int LevelObject::getBarrierAmount() { return amountOfBarriers; }
void LevelObject::setBarrierAmount(int value) { amountOfBarriers = value; }

void LevelObject::createPowerup() {
  //yay, powerups
  powerupX = (int)rand() % 25 * 32;
  powerupY = (int)rand() % 18 * 32;
}

