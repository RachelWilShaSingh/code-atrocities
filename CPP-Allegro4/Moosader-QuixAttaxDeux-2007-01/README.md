Rachel J. Morris [Moosader.com](http://www.moosader.com/)

## ABOUT
This is a small free-for-all top-down game I made.

This was my first game to have a UI, with buttons and such.
Still, it wasn't implemented very well.

## MISSING ASSETS
I deleted assets that I did not own.
Some of my old projects used RPG Maker music/sounds/sprites,
as well as stuff from other places.
