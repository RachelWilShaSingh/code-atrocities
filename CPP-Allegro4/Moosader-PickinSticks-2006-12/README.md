Rachel J. Morris [Moosader.com](http://www.moosader.com/)

## ABOUT
Pickin' Sticks is a one-night game I made based on someone's suggestion
to make a game where you "Pick up sticks endlessly for no raisin".


## MISSING ASSETS
I deleted assets that I did not own.
Some of my old projects used RPG Maker music/sounds/sprites,
as well as stuff from other places.
