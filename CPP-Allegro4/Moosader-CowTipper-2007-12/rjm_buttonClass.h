#pragma once
#include <string>
using namespace std;

class rjm_buttonClass
{
    private:
        int reset_x;
        int reset_y;
        int reset_tileset_x;
        int reset_tileset_y;
        int quit_x;
        int quit_y;
        int quit_tileset_x;
        int quit_tileset_y;
        int width;
        int height;
    public:
        rjm_buttonClass();
        string button_clicked( int mouse_x, int mouse_y );
        void draw( BITMAP *buffer, BITMAP *graphics );
};

rjm_buttonClass::rjm_buttonClass()
{
    reset_x = 725;
    reset_y = 125;
    reset_tileset_x = 800;
    reset_tileset_y = 550;
    quit_x = 610;
    quit_y = 125;
    quit_tileset_x = 924;
    quit_tileset_y = 352;
    width = 50;
    height = 48;
}

void rjm_buttonClass::draw( BITMAP *buffer, BITMAP *graphics )
{
    masked_blit( graphics, buffer, reset_tileset_x, reset_tileset_y, reset_x, reset_y, width, height );
    masked_blit( graphics, buffer, quit_tileset_x, quit_tileset_y, quit_x, quit_y, width, height );
}

string rjm_buttonClass::button_clicked( int mouse_x, int mouse_y )
{
    int mouse_w_h = 10;
    int box_w_h = 40;
    int x, y;
    x = 800-75;
    y = 125;

    if (    ( mouse_x+mouse_w_h >   quit_x ) &&
            ( mouse_x           <   quit_x + width ) &&
            ( mouse_y+mouse_w_h >   quit_y ) &&
            ( mouse_y           <   quit_y + height ) )
    {
        //collision
        return "reset";
    }
    else if (    ( mouse_x+mouse_w_h >   reset_x ) &&
            ( mouse_x           <   reset_x + width ) &&
            ( mouse_y+mouse_w_h >   reset_y ) &&
            ( mouse_y           <   reset_y + height ) )
    {
        //collision
        return "quit";
    }
    return "none";
}

//if ( check_click( mouse_x, mouse_y ) < 0 ) { done = true; }    //quit button
//                    else if ( check_click( mouse_x, mouse_y ) == 1 )  //restart
//                    {
//                        restart_game();
//                    }

//masked_blit( graphics, buffer, 800, 550, 800-75, 125, 50, 48 );
//masked_blit( graphics, buffer, 924, 352, 600+10, 125, 50, 48 );

