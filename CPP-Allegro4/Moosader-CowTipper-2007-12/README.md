Rachel J. Morris, [Moosader.com](http://www.moosader.com/)

## ABOUT
This is a remake of Cow Splode, a game I programmed earlier
in Visual Basic (so I could use Winsock and make it all onliney).
My boyfriend's cousin Natalie, who's about 4 years old,
got a kick out of playing Cow Splode and several of my other
games, so I'm going to make a compilation CD for her!

However, 4 year olds aren't very good with IP addresses and
such so I made this C++ single-computer two-player game 
remake for her, and I made it "cow tipping" instead, so
now it's rated E for Everyone! Yay! No exploding cows!

## CONTROLS 
When the game starts it's the red player's turn.
Simply click a cow and the eight immediately surrounding
cows will be tipped along with the one you clicked.
Then it's blue player's turn.

Reset button resets the match, quit button quits

F4 also quits, and F5 toggles windowed mode.

## WHO DID WHAT
Programming ---Rachel J. Morris
Artwork -------Rachel J. Morris
Sound Effects--RPG Maker
Music----------RPG Maker - thief Legend of Mana - joyful song
Testing--------You

Programmed in C++ using Code::Blocks, and
the Allegro Game Programming Library (http://www.talula.demon.co.uk/allegro/)
Graphics made with Paint Shop Pro 6, except
for character graphics, which was made in Flash 4.0.

## FAQ
All of these questions really were asked to me!
(not)

*Q: What's your favorite cow?!*

A: Cow #144.

*Q: You should make this a flash game so I can play it on my wii!*

A: Hey, good idea!  I don't really like using flash for game making, but
I might just to be able to play this on the wii!

*Q: Are you interested developing in WiiWare or XBLA?*

A: Yes, but only if you buy me the devkit (and an xbox 360). :P


## MISSING ASSETS
I deleted assets that I did not own.
Some of my old projects used RPG Maker music/sounds/sprites,
as well as stuff from other places.
