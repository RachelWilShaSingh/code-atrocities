#pragma once
#include <iostream>
#include <fstream>
#include <allegro.h>
#include "Tile.h"
#include "Brush.h"
#include "global.h"
using namespace std;

class Level
{
    private:
        int xOff, yOff;
        bool grid;
        SAMPLE *confirmSound;
    public:
        Tile ***tile;
//        Level() { Setup(); }
        void Setup();
        void Setup( int, int );
        void Draw( BITMAP*, BITMAP*, int, int );
        void ToggleGrid();
        void MoveOffsetX( int amt ) { xOff += amt; }
        void MoveOffsetY( int amt ) { yOff += amt; }
        int OffsetX() { return xOff; }
        int OffsetY() { return yOff; }
        void OffsetX( int val ) { xOff = val; }
        void OffsetY( int val ) { yOff = val; }
        void CheckClick( Brush*, int );
        void FillLayer( int, int, int, int );
        void ClearLayer( int, int, int );
        void LoadMap( string, int, int );
        void SaveMap( string, int, int );
        ~Level()
        {
            for ( int i=0; i<3; i++ )
            {
                for ( int j=0; j<sizeof( tile[i] ); j++ )
                {
                    for ( int k=0; k<sizeof( tile[i][j] ); k ++)
                    {
                        free( tile[i][j] );
                        tile[i][j] = NULL;
                    }
                }
            }
            delete [] tile;
        }
};







