#include <iostream>
#include <cstdlib>
#include <ctime>
#include <allegro.h>
#include "rjm_bullet.h"
#include "rjm_character.h"
#include "global.h"
using namespace std;

/*
Messy code ahead D:!!
This was made the early-morning of December 18th, just for
fun and for experimental reasons.
*/

volatile long speed_counter = 0;

void increment_speed_counter()
{
    speed_counter++;
}
END_OF_FUNCTION(increment_speed_counter);

void print_annoying_text(BITMAP*, float);
void init_crap(bool);

int main(int argc, char *argv[])
{
    bool fullscreen = false;
    init_crap(fullscreen);

    BITMAP *buffer = create_bitmap(scr_w,scr_h);
//    BITMAP *background = load_bitmap("space.bmp", NULL);
//    SAMPLE *sound_shoot = load_sample("sound1.wav");
//    SAMPLE *sound_die = load_sample("sound2.wav");
//    SAMPLE *sound_enemy_die = load_sample("sound3.wav");
    BITMAP *title = load_bitmap("content/title.bmp", NULL);
//    MIDI *song = load_midi("ogrebattle.mid");
    RJM_CHARACTER daphne;
    RJM_CHARACTER baddie[10];
    float offset = 0;           //The background's offset (only moves vertically)
    int barrier_count = 2;
    int lives = 10;
    for (int i=0; i<10; i++)    //make all baddies random types and such
    {
        baddie[i].randomize();
        baddie[i].active = false;
    }
    daphne.image = load_bitmap("content/daphnee.bmp", NULL);
    bool done = false;
    float counter;              //Random counter to time the animations so they're not really fast
    float counterB = 0;         //Other counter to determine how often to make a new baddie
    int daph_counter = 0;       //Counter for Daphne's bullet-shield/resurrection shield
    bool game_over = false;
    int esc_counter = 0;    //Counter to make sure if you hit esc it doesn't log keypresses very quickly
                            //(and therefore switch from paused to not paused and make it hard to be in one mode)
    int score = 0;
    cout<<daphne.active;
//    play_midi(song, true);
    bool pause = true;
    daphne.active = true;

    if ( !title )
    {
        cout << "Title error" << endl;
    }
//    if ( !background )
//    {
//        cout << "Background error" << endl;
//    }
    if ( !daphne.image )
    {
        cout << "Daphne error" << endl;
    }

    while (!done)
    {
        while (speed_counter > 0)
        {
            if (key[KEY_ESC])
            {
                if (pause && esc_counter <= 0)
                {
                    pause = false;
                    esc_counter = 50;
                }
                else if ( pause == false && esc_counter <= 0 )
                {
                    pause = true;
                    esc_counter = 50;
                }
            }
            if (key[KEY_F5])
            {
                if ( fullscreen ) { set_gfx_mode(GFX_AUTODETECT_WINDOWED, scr_w, scr_h, 0, 0); fullscreen = false; }
                else { set_gfx_mode(GFX_AUTODETECT, scr_w, scr_h, 0, 0); fullscreen = true; }
            }
            if (key[KEY_F4]) { done = true; }
            if ( !pause )
            {
                if ((key[KEY_UP] || key[KEY_W]) && daphne.active == true)
                {
                    if ( daphne.y - daphne.speed >= 0 )
                    {
                        daphne.y -= daphne.speed;
                        offset += 0.9;
                    }
                }
                else if ((key[KEY_DOWN] || key[KEY_S]) && daphne.active == true)
                {
                    if ( daphne.y + daphne.speed <= 800+48 )
                    {
                        daphne.y += daphne.speed;
                        offset -= 0.9;
                    }
                }
                if ((key[KEY_LEFT] || key[KEY_A]) && daphne.active == true)
                {
                    if ( daphne.x - daphne.speed >= 0 )
                        daphne.x -= daphne.speed;
                }
                else if ((key[KEY_RIGHT] || key[KEY_D]) && daphne.active == true)
                {
                    if ( daphne.x + daphne.speed <= 600+48 )
                        daphne.x += daphne.speed;
                }
                if ((key[KEY_ENTER] || key[KEY_F]) && daphne.active == true && barrier_count > 0 && daph_counter <= 0)        //Barrier
                {
                    daph_counter = 50;
                    barrier_count--;
                }
                if ( counter < 0.1 )
                {
                    if (key[KEY_SPACE] && daphne.active == true)
                    {
                        for (int i=0; i<20; i++)
                        {
                            if ( daphne.p_bullet[i].active == false ) //shoot
                            {
                                daphne.p_bullet[i].x = daphne.x + daphne.w;
                                daphne.p_bullet[i].y = daphne.y + daphne.w/2;
                                daphne.p_bullet[i].active = true;
                                daphne.shooting = (int)counter;
//                                play_sample(sound_shoot, 255, 128, 1000, false);
                                //play_sample(sample, volume, pan, frequency, loop)
                                // 255 = max. volume,
                                // 128 = pan in center (0 is all sound played on left, 255 is all on right)
                                // 1000 = normal frequency, 500 will be half-rate and such
                                // false = loop sound
                                // http://oregonstate.edu/~barnesc/quick_reference.html is a good website
                                break;
                            }
                        }
                    }
                }

                for (int i=0; i<20; i++)        //move player bullets
                {
                    if ( daphne.p_bullet[i].active == true )
                    {
                        daphne.p_bullet[i].x += daphne.p_bullet[i].speed;
                        if ( daphne.p_bullet[i].x >= 800 ) { daphne.p_bullet[i].active = false; }
                    }
                }
                int temp_spawn;
                if ( (int)counterB % 10 == 1 )   //new baddie
                {
                    for (int i=0; i<10; i++)
                    {
                        if ( baddie[i].active == false )
                        {
                            temp_spawn = (int)rand() % 5;
                            if ( temp_spawn >= 3 )    //If number is 3 or 4, then spawn a baddie
                            {
                                baddie[i].randomize();
                                baddie[i].active = true;
                                break;
                            }
                        }
                    }
                }
                for (int i=0; i<10; i++)        //move baddies
                {
                    if ( baddie[i].active == true )
                    {
                        baddie[i].dostuff();
                        baddie[i].x -= baddie[i].speed;
                        if (baddie[i].x < -48) { baddie[i].active = false; baddie[i].x = 800; }
                    }
                    for (int j=0; j<max_enemy_bullets; j++)     //move baddie bullets
                    {
                        if ( baddie[i].b_bullet[j].active == true )
                        {
                            baddie[i].b_bullet[j].x -= baddie[i].b_bullet[j].speed;
                            if ( baddie[i].b_bullet[j].x < -16 ) { baddie[i].b_bullet[j].active = false; }
                        }
                    }
                }
                //check collision
                if (daphne.active == true)
                {
                    for (int i=0; i<10; i++)
                    {
                        //baddies
                        if ((daphne.y+daphne.h-5  >    baddie[i].y ) &&             //the "-5" is to chop off some of the blank space on the sprite
                            (daphne.y+5           <    baddie[i].y+baddie[i].h ) &&
                            (daphne.x+daphne.w  >    baddie[i].x ) &&
                            (daphne.x           <    baddie[i].x+baddie[i].w ) &&
                            baddie[i].active == true && daph_counter <= 0)          //Too lazy for pixel-perfect
                        {
                            daphne.active = false;
                            daphne.die_time = counter;
                            baddie[i].active = false;
                            lives--;
//                            play_sample(sound_die, 255, 128, 1000, false);
                            daph_counter = 100;
                        }
                        else
                        {
                            for (int j=0; j<max_enemy_bullets; j++)
                            {
                                if ((daphne.y+daphne.h-5  >    baddie[i].b_bullet[j].y ) &&
                                    (daphne.y+5           <    baddie[i].b_bullet[j].y+baddie[i].b_bullet[j].h ) &&
                                    (daphne.x+daphne.w  >    baddie[i].b_bullet[j].x ) &&
                                    (daphne.x           <    baddie[i].b_bullet[j].x+baddie[i].b_bullet[j].w ) &&
                                    baddie[i].b_bullet[j].active == true && daph_counter <= 0)
                                {
                                    daphne.active = false;
                                    daphne.die_time = counter;
                                    baddie[i].b_bullet[j].active = false;
                                    lives--;
//                                    play_sample(sound_die, 255, 128, 1000, false);
                                    daph_counter = 100;
                                }
                            }
                        }
                    }
                }
                for (int i=0; i<20; i++)
                {
                    for (int j=0; j<10; j++)
                    {
                        if ((daphne.p_bullet[i].y+daphne.p_bullet[i].h  >    baddie[j].y ) &&
                            (daphne.p_bullet[i].y                     <    baddie[j].y+baddie[j].h ) &&
                            (daphne.p_bullet[i].x+daphne.p_bullet[i].w  >    baddie[j].x ) &&
                            (daphne.p_bullet[i].x                     <    baddie[j].x+baddie[j].w ) &&
                            baddie[j].active == true && daphne.p_bullet[i].active == true)
                        {
                            score++;
                            baddie[j].active = false;
//                            play_sample(sound_enemy_die, 255, 128, 1000, false);
                        }
                    }
                }
                counterB += 9;
            }
            if (daph_counter > 0) { daph_counter--; }   //barrier
            counter += 0.1;                             //animation counter
            if ( esc_counter > 0 ) { esc_counter--; }   //esc key toggle counter (I should have named these better)
            speed_counter--;
            if ( counter >= 2.0 ) { counter = 0.0; }
            if ( daphne.shooting != -1 )                //-1 means she's not shooting
            {
                if ( daphne.shooting + 1 == (int)counter )
                {
                    daphne.shooting = -1;
                }
            }
            if ( daphne.active == false && daphne.die_time + 3 >= counter )
            {
                daphne.x = 0;
                daphne.y = scr_h/2 - daphne.h/2;
                daphne.active = true;
            }
            if ( lives <= 0 )
            {
                pause = true;
                game_over = true;
            }
            if ( score%10 == 0 && score > 0 )
            {
                if ( daphne.speed < 4 )
                {
                    daphne.speed += 1;
                    for (int i=0; i<20; i++) { daphne.p_bullet[i].speed += 0.2; }
                }
            }
        }//while (speed_counter > 0)
        text_mode(-1);
        if ( !game_over )
        {
//            draw_sprite(buffer, background, 0, -300+offset);
            if (daphne.active == true)
                daphne.draw(buffer, counter);
            for (int i=0; i<20; i++)
            {
                if ( daphne.p_bullet[i].active == true )
                {
                    daphne.p_bullet[i].draw(buffer, counter);
                }
                if ( i < 10 )
                {
                    if ( baddie[i].active == true )
                    {
                        baddie[i].draw(buffer, counter);
                    }
                    for (int j=0; j<max_enemy_bullets; j++)
                    {
                        if ( baddie[i].b_bullet[j].active == true )
                        {
                            baddie[i].b_bullet[j].draw(buffer, counter);
                        }
                    }
                }
            }
            if ( daph_counter > 0 )     //This is her respawn barrier / bullet-shield
            {
                circle(buffer, daphne.x+32, daphne.y+24, daph_counter-1, makecol(0, 0, 0));
                circle(buffer, daphne.x+32, daphne.y+24, daph_counter, makecol(0, 255, 0));
                circle(buffer, daphne.x+32, daphne.y+24, daph_counter+1, makecol(0, 0, 0));
            }
            textprintf_centre(buffer, font, 100, 5, makecol(255, 0, 0), "LIVES LEFT: %i", lives);
            textprintf_centre(buffer, font, 700, 5, makecol(0, 255, 0), "BARRIERS LEFT: %i", barrier_count);
            textprintf_centre(buffer, font, scr_w/2, 5, makecol(0, 100, 255), "SCORE: %i", score);
            if ( pause )
            {
                draw_sprite(buffer, title, scr_w/2 - 175, 100);
                print_annoying_text(buffer, counter);
            }
        }
        else
        {
            rectfill(buffer, 0, 0, scr_w, scr_h, makecol(50, 0, 0));
            textprintf_centre(buffer, font, scr_w/2, scr_h/2-100, makecol(255, 0, 255), "----SCORE: %i----", score);
            textprintf_centre(buffer, font, scr_w/2, scr_h/2-5, makecol(255, 0, 0), "DAPHNE DIED A TRAGIC DEATH IN SPACE");
            textprintf_centre(buffer, font, scr_w/2, scr_h/2+5, makecol(255, 0, 0), "BECAUSE OF AN INCOMPETENT PILOT!");
            textprintf_centre(buffer, font, scr_w/2, scr_h/2+100, makecol(255, 0, 0), "press F4 to quit");
        }
        acquire_screen();
        blit(buffer, screen, 0, 0, 0, 0, scr_w, scr_h);
        clear_bitmap(buffer);
        release_screen();
    }//while (!key[KEY_ESC])

    return 0;
}
END_OF_MAIN();

void print_annoying_text(BITMAP *buffer, float counter)
{
    int blackcolor = makecol(0, 0, 0);
    int tx = scr_w/2;
    int ty = scr_h/2;
    int cntr = ((int)counter * 10);
    int textcolor = makecol(0, 255, 100);
    textprintf_centre(buffer, font, tx, ty-10+1, blackcolor, "PAUSED");
    textprintf_centre(buffer, font, tx, ty+12+1, blackcolor, "CONTROLS:");
    textprintf_centre(buffer, font, tx, ty+24+1, blackcolor, "WASD OR ARROW KEYS TO MOVE");
    textprintf_centre(buffer, font, tx, ty+36+1, blackcolor, "SPACEBAR TO SHOOT");
    textprintf_centre(buffer, font, tx, ty+48+1, blackcolor, "F OR ENTER TO USE BULLET-SHIELD");
    textprintf_centre(buffer, font, tx, ty+60+1, blackcolor, "ESC TO UNPAUSE");
    textprintf_centre(buffer, font, tx, ty+72+1, blackcolor, "F5 TO TOGGLE FULLSCREEN MODE");
    textprintf_centre(buffer, font, tx, ty+84+1, blackcolor, "F4 TO QUIT");

    textprintf_centre(buffer, font, tx, ty-10-1, blackcolor, "PAUSED");
    textprintf_centre(buffer, font, tx, ty+12-1, blackcolor, "CONTROLS:");
    textprintf_centre(buffer, font, tx, ty+24-1, blackcolor, "WASD OR ARROW KEYS TO MOVE");
    textprintf_centre(buffer, font, tx, ty+36-1, blackcolor, "SPACEBAR TO SHOOT");
    textprintf_centre(buffer, font, tx, ty+48-1, blackcolor, "F OR ENTER TO USE BULLET-SHIELD");
    textprintf_centre(buffer, font, tx, ty+60-1, blackcolor, "ESC TO UNPAUSE");
    textprintf_centre(buffer, font, tx, ty+72-1, blackcolor, "F5 TO TOGGLE FULLSCREEN MODE");
    textprintf_centre(buffer, font, tx, ty+84-1, blackcolor, "F4 TO QUIT");

    textprintf_centre(buffer, font, tx-1, ty-10, blackcolor, "PAUSED");
    textprintf_centre(buffer, font, tx-1, ty+12, blackcolor, "CONTROLS:");
    textprintf_centre(buffer, font, tx-1, ty+24, blackcolor, "WASD OR ARROW KEYS TO MOVE");
    textprintf_centre(buffer, font, tx-1, ty+36, blackcolor, "SPACEBAR TO SHOOT");
    textprintf_centre(buffer, font, tx-1, ty+48, blackcolor, "F OR ENTER TO USE BULLET-SHIELD");
    textprintf_centre(buffer, font, tx-1, ty+60, blackcolor, "ESC TO UNPAUSE");
    textprintf_centre(buffer, font, tx-1, ty+72, blackcolor, "F5 TO TOGGLE FULLSCREEN MODE");
    textprintf_centre(buffer, font, tx-1, ty+84, blackcolor, "F4 TO QUIT");

    textprintf_centre(buffer, font, tx+1, ty-10, blackcolor, "PAUSED");
    textprintf_centre(buffer, font, tx+1, ty+12, blackcolor, "CONTROLS:");
    textprintf_centre(buffer, font, tx+1, ty+24, blackcolor, "WASD OR ARROW KEYS TO MOVE");
    textprintf_centre(buffer, font, tx+1, ty+36, blackcolor, "SPACEBAR TO SHOOT");
    textprintf_centre(buffer, font, tx+1, ty+48, blackcolor, "F OR ENTER TO USE BULLET-SHIELD");
    textprintf_centre(buffer, font, tx+1, ty+60, blackcolor, "ESC TO UNPAUSE");
    textprintf_centre(buffer, font, tx+1, ty+72, blackcolor, "F5 TO TOGGLE FULLSCREEN MODE");
    textprintf_centre(buffer, font, tx+1, ty+84, blackcolor, "F4 TO QUIT");

    textprintf_centre(buffer, font, tx, ty-10, textcolor+cntr, "PAUSED");
    textprintf_centre(buffer, font, tx, ty+12, textcolor-cntr, "CONTROLS:");
    textprintf_centre(buffer, font, tx, ty+24, textcolor+cntr, "WASD OR ARROW KEYS TO MOVE");
    textprintf_centre(buffer, font, tx, ty+36, textcolor-cntr, "SPACEBAR TO SHOOT");
    textprintf_centre(buffer, font, tx, ty+48, textcolor+cntr, "F OR ENTER TO USE BULLET-SHIELD");
    textprintf_centre(buffer, font, tx, ty+60, textcolor-cntr, "ESC TO UNPAUSE");
    textprintf_centre(buffer, font, tx, ty+72, textcolor+cntr, "F5 TO TOGGLE FULLSCREEN MODE");
    textprintf_centre(buffer, font, tx, ty+84, textcolor-cntr, "F4 TO QUIT");
}

void init_crap(bool fullscreen)
{
    srand((unsigned)time(NULL));
    allegro_init();
    install_keyboard();
    install_timer();
    install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, 0);

    LOCK_VARIABLE(speed_counter);
    LOCK_FUNCTION(increment_speed_counter);
    install_int_ex(increment_speed_counter,BPS_TO_TIMER(90));

    set_color_depth(16);
    if ( fullscreen )
        set_gfx_mode(GFX_AUTODETECT, scr_w, scr_h, 0, 0);
    else
        set_gfx_mode(GFX_AUTODETECT_WINDOWED, scr_w, scr_h, 0, 0);
}
