Rachel J. Morris [Moosader.com](http://www.moosader.com/)

## ABOUT
This is a small game I made where you pick up pokemon and throw them in a fire.

## MISSING ASSETS
I deleted assets that I did not own.
Some of my old projects used RPG Maker music/sounds/sprites,
as well as stuff from other places.
