#include "Game.h"

Game::Game()
{
	maxFPS = 60;
	screenWidth = 800;
	screenHeight = 600;
	screenBPP = 32;
	SDL_Init( SDL_INIT_EVERYTHING );
	SDL_WM_SetCaption( "Pickin' Online Sticks v0.1", NULL );
	buffer = NULL;
	buffer = SDL_SetVideoMode( screenWidth, screenHeight, screenBPP, SDL_SWSURFACE );
	if ( buffer == NULL ) { cerr<<"Error initializing the buffer\n"<<endl; }
}
Game::~Game()
{
	SDL_FreeSurface( buffer );
	SDL_Quit();
}

int Game::ScreenWidth() { return screenWidth; }
int Game::ScreenHeight() { return screenHeight; }
int Game::ScreenBPP() { return screenBPP; }
int Game::FPS() { return maxFPS; }

void Game::SetWindowCaption( string caption )
{
    SDL_WM_SetCaption( caption.c_str(), NULL );
}

void Game::Update()
{
	SDL_Flip( buffer );
}
