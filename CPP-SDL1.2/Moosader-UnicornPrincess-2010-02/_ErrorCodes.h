/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

_ErrorCodes.h
*/

#pragma once

#define SDL_INIT_ERROR 9
#define BUFFER_INIT_ERROR 10
#define FILEPATH_ERROR 11
#define INDEX_ERROR 12
#define RES_DIMENSION_BAD 13
#define MISSING_ENEMY_INFO 14
#define SZID_ERROR 15
#define SOUND_INIT_ERROR 16
