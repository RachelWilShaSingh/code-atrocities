/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Character.h
*/

#pragma once

#include <math.h>

#include "_ErrorCodes.h"
#include "_Global.h"

#include "BulletManager.h"
#include "ImageManager.h"
#include "SoundManager.h"

#include "BaseObj.h"

using namespace std;

class Character : public BaseObj
{
    protected:
        float frame;                    // Current frame of animation
        float shootCooldownLeft;        // Limiting the rate the character can shoot bullets.
        float shootCooldownTime;        // Max. amount of time for shooting cooldown
        BulletType bulletType;          // Type of bullets character shoots (See: _Global)
        BulletDamage gunDamage;         // Type of damage the bullets do (player, enemy, all) (See: _Global)
    public:
        // Initialization / Constructors / Destructors
        Character();
        void Init();
        void Draw();

        // Misc Functions
        void IncrementFrame();
        void Update();
        void Shoot();
        void Die();
};
