/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

ImageManager.h
*/

#pragma once
#include <iostream>
#include <fstream>

#include "_ErrorCodes.h"
#include "_Global.h"

#include "Image.h"

using namespace std;

class ImageManager
{
    private:
        // Singleton instance
        static ImageManager *inst;
        ImageManager();

        int bgScroll;           // "BG Scroll".  Scrolls and loops background image
        int absScroll;          // "Absolute Scroll". Used to determine when to switch levels.
        int currentBgImg;       // The level's current background image. Temp: Store in LevelManager
        int currentLevel;       // temp
        float levelNameTimer;   // temp
    public:
        // Singleton functions
        static ImageManager& GetInst();
        static ImageManager* InstPtr();

        // Initialization / Constructors / Destructors
        void Init( string& gfxDir );

        // Get / Set & other accessor functions
        void SetBackgroundImage( int val );
        void SetBackgroundImage( string val );
        int BgScroll();
        int AbsScroll();
        // Misc functions
        void ResetLevelVariables();
        void ShowTitle( int lvl );

            // Drawing-related functions
        void DrawBackground();
        void DrawLevelName();
        void DrawImage( const SDL_Rect& coordDestination, const SDL_Rect& coordCrop, int iID );
        void DrawImage( const SDL_Rect& coordDestination, int iID );
        void DrawImage( const SDL_Rect& coordDestination, const SDL_Rect& coordCrop, const string& szID );
        void DrawImage( int x, int y, const char* );
        void DrawToScreen();
        void ClearBgRect( SDL_Rect *coordinates );

            // Scrolley functions
        void ScrollScreen();
        int ReturnIntId( string szID );

        // Class variables
        SDL_Surface *buffer;
        Image image[IMGAMT];      // Temp: Change to private
};


