/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

ItemManager.cpp
*/

#include "ItemManager.h"

// Singleton intialization accessors
ItemManager& ItemManager::GetInst()
{
    if ( !inst )
        inst = new ItemManager();

    return *inst;
}

ItemManager* ItemManager::InstPtr()
{
    if ( !inst )
        inst = new ItemManager();

    return inst;
}

// Initialization / Constructors / Destructors
ItemManager::ItemManager()
{
    ;
}

void ItemManager::Init()
{
    itemCount = 0;
}

int ItemManager::ItemCount()
{
    return itemCount;
}

void ItemManager::CreateItem( int x, int y, BulletType bullet )
{
    if ( item == NULL )
    {
        // Array empty, allocate space and create item.
        itemCount++;
        item = new Item[itemCount];

        item[itemCount-1].Init( x, y, bullet );
    }
    else
    {
        // Array already has one or more element(s).  Resize array and create item.
        AddOneToItemArray();

        item[itemCount-1].Init( x, y, bullet );
    }
}

void ItemManager::UpdateItems()
{
    for ( int i=0; i<itemCount; i++ )
    {
        item[i].Update();
    }
}

void ItemManager::AddOneToItemArray()
{
    // Allocate space in array for new item
    Item* tempArray = NULL;
    tempArray = new Item[itemCount];

    for ( int i=0; i<itemCount; i++ )
    {
        tempArray[i] = item[i];
    }

    delete [] item;
    item = NULL;
    itemCount++;
    item = new Item[itemCount];

    for ( int i=0; i<itemCount-1; i++ )
    {
        item[i] = tempArray[i];
    }

    delete [] tempArray;
    tempArray = NULL;
}

void ItemManager::DrawItems()
{
    for ( int i=0; i<itemCount; i++ )
    {
        if ( item[i].Active() == true ) // TEMP: Change so it's either in the manager, or item, but not both
        {
            item[i].Draw();
        }
    }
}
