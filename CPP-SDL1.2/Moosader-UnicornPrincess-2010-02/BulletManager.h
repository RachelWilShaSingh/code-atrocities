/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

BulletManager.h
*/

#pragma once

#include "_ErrorCodes.h"
#include "_Global.h"

#include "Bullet.h"

using namespace std;

class BulletManager
{
    private:
        // Singleton instance
        static BulletManager* inst;
        BulletManager() { ; }
    public:
        // Singleton functions
        static BulletManager& GetInst();
        static BulletManager* InstPtr();

        // Initialization / Constructors / Destructors
        void Init();

        // Get / Set & other accessor functions

        // Misc functions
        void CreateBullet( int x, int y, Direction dir );
        void UpdateBullets();
        void DrawBullets();
        void Shoot( int x, int y, Direction dir, BulletType type, BulletDamage damage  );

        // Class variables
        Bullet bullet[MAX_BULLETS];      // Temp: Change to private
};
