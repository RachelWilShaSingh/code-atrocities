/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Enemy.cpp
*/

#include "Enemy.h"

Enemy::Enemy()
{
    SetCoordinates( -3, -3, 3, 3 );
    SetColReg( -3, -3, 3, 3 );
    active = false;
}

void Enemy::Init( string& type, int x, int y )
{
    SetCoordinates( x, y, 128, 128 );
    active = true;

    gunDamage = PLAYER;

    imgId = ImageManager::GetInst().ReturnIntId( type );
    enemyType = (EnemyType)imgId;

    if ( enemyType == SCOUT )
    {
        movementType = WAVE;
        bulletType = bSMALLBULLET;
         SetColReg( 41, 19, 45, 91 );
         shootCooldownTime = 50;
    }
    else if ( enemyType == TROOPER )
    {
        movementType = TOWARDS_PLAYER;
        bulletType = bBULLET;
        SetColReg( 29, 36, 74, 70 );
        shootCooldownTime = 10;
    }

    speed = 2;
    defSpeed = 2;
    sightDistance = 800;

    action = FLYING;
    frame = 0;
    dir = LEFT;
}

void Enemy::Update( Player& player )
{
    /* Update enemy - move it and decide whether to shoot */
    DecidePath( player.CoordPtr() );
    DecideShoot( player );
    IncrementFrame();

    if ( shootCooldownLeft > 0 )
    {
        shootCooldownLeft -= 1;
    }
}

void Enemy::DecidePath( SDL_Rect& playerCoord )
{
    // Temp: Clean up
    if ( active && !dying )
    {
        /* Move npc based on it's type */
        speed = defSpeed;
        Move( LEFT );

        // Vertical movement
        switch ( movementType )
        {
            case LINE:
                // No Y Movement
            break;
            case TOWARDS_PLAYER:
                // Don't start moving around until near player
                if ( DistanceTo( playerCoord ) <= sightDistance )
                {
                    // Move towards
                    if ( playerCoord.x < coord.x )  // Don't bother moving if you've already passed the player
                    {
                        if ( playerCoord.y < coord.y )
                        {
                            speed = 1;
                            Move( UP );
                        }
                        else if ( playerCoord.y > coord.y )
                        {
                            speed = 1;
                            Move( DOWN );
                        }
                    }
                }
            break;
            case AWAY_PLAYER:
                // Don't start moving around until near player
                if ( DistanceTo( playerCoord ) <= sightDistance )
                {
                    if ( playerCoord.x < coord.x )  // Don't bother moving if you've already passed the player
                    {
                        // Move towards
                        if ( playerCoord.y < coord.y )
                        {
                            speed = 1;
                            Move( DOWN );
                        }
                        else if ( playerCoord.y > coord.y )
                        {
                            speed = 1;
                            Move( UP );
                        }
                    }
                }
            break;
            case WAVE:
                speed = (int)(GameSys::GetInst().GameTime()) % 4;
                if ( (int)(GameSys::GetInst().GameTime()) % 8 < 4 )
                {
                    Move( DOWN );
                }
                else
                {
                    Move( UP );
                }
            break;
        }
    }
}

void Enemy::DecideShoot( Player& player )
{
    // if you haven't shot the maximum amount of bullets
    // and you're lined up with the player (within a range)
    // then shoot!
    if ( ( player.X() <= coord.x ) // Only shoot if player is to the left of the enemy
            && ( DistanceTo( player.CoordPtr() ) <= 900 )  // Don't shoot if they're more than this distance away
            && (player.Y()-64 < coord.y) && (player.Y()+64 > coord.y) // If the player and enemy's Y coordinates are within a small region
            && player.Active() == true && active == true && !dying ) // If both player and enemy are active, and the enemy is not dying.
    {
        Shoot();
    }
}

void Enemy::Die()
{
    if ( active && !dying )
    {
        frame = 0.0;
        dying = true;
        action = DYING;

        // Randomly decide to give items
        int dropItem = (rand() % 100); // 30% of time they drop items.

        if ( dropItem < 10)
        {
            ItemManager::GetInst().CreateItem( coord.x, coord.y, bGEM );
        }
        else if ( dropItem < 20 )
        {
            ItemManager::GetInst().CreateItem( coord.x, coord.y, bHEART );
        }
        else if ( dropItem < 30 )
        {
            ItemManager::GetInst().CreateItem( coord.x, coord.y, bFLOWER );
        }
        // Otherwise, drop nothing.
    }
}

