/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Text.h
*/

#pragma once

#include <iostream>
#include <string>

#include "_ErrorCodes.h"
#include "_Global.h"

using namespace std;

void InitFont( string filename, int size );

void DrawText( SDL_Surface* destination, string& message, float x, float y, float offsetX, float offsetY, int size, SDL_Color color );

void DrawText( SDL_Surface* destination, string& message, float x, float y, float offsetX, float offsetY, int size, int r, int g, int b );

void DrawText( SDL_Surface* destination, char* message, float x, float y, float offsetX, float offsetY, int size, int r, int g, int b );

