/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

BaseObj.h
The base object that the Character and Bullet class inherit from
(And the Player, Item and Enemy class inherit from Character)
*/

#pragma once

#include <math.h>

#include "_ErrorCodes.h"
#include "_Global.h"

#include "GameSys.h"
#include "ImageManager.h"

using namespace std;

const int MAXFRAME = 4;

class BaseObj
{
    protected:
        SDL_Rect coord;     // x, y, w, h
        SDL_Rect colReg;    // Collision region
        float speed;        // Movement speed
        Direction dir;      // Direction it's moving
        Action action;      // Action (See: _Global)
        int imgId;          // the iID (See: ImageManager) for its image
        bool active;        // If it's active (if it's not, most functions will skip over, like Draw and Update)
        bool dying;         // For the dying animation.  Temp: Get rid of
    public:
        // Initialization / Constructors / Destructors
        BaseObj();
        void Init();

        // Get / Set & other accessor functions
        bool Active();
        int X();
        int Y();
        int W();
        int H();
        float Speed();
        SDL_Rect Coord();
        SDL_Rect& CoordPtr();
        SDL_Rect ColReg();
        SDL_Rect CollisionCoord();
        void SetCoordinates( int x, int y, int w, int h );
        void SetCoordinates( int x, int y );
        void SetColReg( int x, int y, int w, int h );

        // Misc functions
        void Move( Direction direction );
        void Draw();
        void Update();
        float DistanceTo( SDL_Rect& coord );

        // Temp
        void DisplayStats();
};
