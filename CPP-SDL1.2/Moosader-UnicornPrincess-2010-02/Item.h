/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Item.h
*/

#pragma once
#include <iostream>
#include <fstream>
#include <string>

#include "_ErrorCodes.h"
#include "_Global.h"

#include "ImageManager.h"

#include "Character.h"

using namespace std;

class Item : public Character
{
    private:
        ItemType itemType;      // Type of item (See: _Global)
    public:
        // Initialization
        void Init( int x, int y, BulletType bullet );

        // Get/Set
        BulletType GetBulletType();
        ItemType GetItemType();

        // Misc functions
        void Update();
        void Draw();
};

