/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Item.cpp
*/


#include "Item.h"

void Item::Init( int x, int y, BulletType bullet )
{
    SetCoordinates( x, y, 48, 48 );
    SetColReg( 0, 0, 48, 48 );

    imgId = 8;
    bulletType = bullet;
    speed = 2.0;
    frame = 0;

    // What kind of gun does it give when picked up?
    if ( bullet == bGEM )
    {
        itemType = iGEM;
    }
    else if ( bullet == bFLOWER )
    {
        itemType = iFLOWER;
    }
    else if ( bullet == bHEART )
    {
        itemType = iHEART;
    }

    active = true;
}

BulletType Item::GetBulletType() { return bulletType; }

ItemType Item::GetItemType() { return itemType; }

void Item::Update()
{
    if ( active )
    {
        IncrementFrame();
        Move( LEFT );

        if ( coord.x < 0 - coord.w )
        {
            active = false;
        }
    }
}

void Item::Draw()
{
    // void DrawImage( const SDL_Rect *coordDestination, const SDL_Rect *coordCrop, int iID );
    if ( active )
    {
        SDL_Rect coordCrop;
        coordCrop.x = (int)frame * coord.w;
        coordCrop.y = (int)itemType * coord.h;
        coordCrop.w = coord.w;
        coordCrop.h = coord.h;

        ImageManager::GetInst().DrawImage( coord, coordCrop, imgId );
    }
}



