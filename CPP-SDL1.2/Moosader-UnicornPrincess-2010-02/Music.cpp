/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Music.cpp
*/

#include "Music.h"

Music::~Music()
{
    if ( msc )
        Mix_FreeMusic( msc );
}

void Music::Init( int index, string& sndDir )
{
    iID = index;

    switch( iID )
    {
        case 0:
            szID = "Theme";
        break;

        case 1:
            szID = "Melody";
        break;

        default:
            szID = "ERROR";
        break;
    }

    if ( szID != "ERROR" )
    {
        LoadMusic( sndDir );
    }
    else
    {
        cerr<<"Error "<<INDEX_ERROR<<": Error loading in music: "<<iID<<", out of bounds."<<endl;
    }
}

void Music::LoadMusic( string& sndDir )
{
    string filepath = sndDir;
    filepath += szID;
    filepath += ".ogg";

    msc = Mix_LoadMUS( filepath.c_str() );

    if ( msc != NULL )
    {
        cout<<"Loaded music "<<filepath<<" successfully"<<endl;
    }
    else
    {
        cerr<<"Error "<<FILEPATH_ERROR<<": Error loading in music # "<<iID<<" at "<<filepath<<endl;
    }
}

string Music::StrID() { return szID; }

int Music::IntID() { return iID; }


