/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Sound.cpp
*/

#include "Sound.h"


Sound::~Sound()
{
    if ( snd )
        Mix_FreeChunk( snd );
}

void Sound::Init( int index, string& sndDir )
{
    iID = index;

    switch( iID )
    {
        case 0:
            szID = "MachineGun";
        break;

        case 1:
            szID = "Jingle";
        break;

        case 2:
            szID = "Pistol";
        break;

        default:
            szID = "ERROR";
        break;
    }

    if ( szID != "ERROR" )
    {
        LoadSound( sndDir );
    }
    else
    {
        cerr<<"Error "<<INDEX_ERROR<<": Error loading in sound: "<<iID<<", out of bounds."<<endl;
    }
}

void Sound::LoadSound( string& sndDir )
{
    string filepath = sndDir;
    filepath += szID;
    filepath += ".ogg";

    snd = Mix_LoadWAV( filepath.c_str() );  // Temp: Change to OGG

    if ( snd != NULL )
    {
        cout<<"Loaded sound "<<filepath<<" successfully"<<endl;
    }
    else
    {
        cerr<<"Error "<<FILEPATH_ERROR<<": Error loading in sound # "<<iID<<" at "<<filepath<<endl;
    }
}

string Sound::StrID() { return szID; }

int Sound::IntID() { return iID; }

