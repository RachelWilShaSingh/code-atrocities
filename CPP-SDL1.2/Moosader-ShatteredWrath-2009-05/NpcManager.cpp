#include "NpcManager.h"

NpcManager::NpcManager( LuaManager *luaScript )
{
    DebugOutput( "NpcManagerConstructor" );
    /* set up NPCs */
    npcCount = 7;
    for ( int i=0; i<npcCount; i++ )
    {
        DebugOutput( "\nNPC", i );
        DebugOutput( "Setup" );
        npc[i].Setup( i, luaScript );

        DebugOutput( "AddUniqueNpc" );
        DebugOutput( "Name", npc[i].Name() );
        DebugOutput( "Type", npc[i].Type() );
        luaScript->AddUniqueNpc( npc[i].Name().c_str(), npc[i].Type().c_str() );
    }
}

NpcManager::~NpcManager()
{
}

void NpcManager::DrawNpcs( SDL_Surface *destination, ImageManager *img, float offsetX, float offsetY, int currentLevel )
{
    DebugOutput( "DrawNpcs" );
    for ( int i=0; i<npcCount; i++ )
    {
        npc[i].DrawNpc( destination, img->enemy[ npc[i].Image() ], (int)offsetX, (int)offsetY, currentLevel );
    }
}

void NpcManager::Update( Level *level, LuaManager *luaScript )
{
    DebugOutput( "\nNpcManager::Update()" );
    Direction tempDir;
    for ( int i=0; i<npcCount; i++ )
    {
        tempDir = npc[i].Update( luaScript );
        DebugOutput( "NpcManager::Update() - tempDir = ", (int)tempDir );
        DebugOutput( "NpcManager::Update() > Check IsCollision between NPC and level " );
        if ( IsCollision( &npc[i], level ) )
        {
            //move back to original position.
            if ( tempDir == UP ) { npc[i].Correct( DOWN ); }
            else if ( tempDir == DOWN ) { npc[i].Correct( UP ); }
            else if ( tempDir == LEFT ) { npc[i].Correct( RIGHT ); }
            else if ( tempDir == RIGHT ) { npc[i].Correct( LEFT ); }
        }
    }
}

string NpcManager::TryInteraction( Character *player, LuaManager *luaScript )
{
    DebugOutput( "TryInteraction" );
    // Check if any NPCs are nearby player
    for ( int i=0; i<npcCount; i++ )
    {
        if ( Nearby( player, &npc[i] ) )
        {
            //Check if player is facing NPC.
            if ( FacingNpc( player, &npc[i] ) )
            {
                return npc[i].GetDialog( luaScript );
            }
        }
    }
    return "NULL";
}

bool NpcManager::Nearby( Character *chara1, Character *chara2 )
{
    DebugOutput( "Nearby" );
    int regionSize = 16;
    /*
    Check eight surrounding tiles around chara1
    */

    if (    ( chara2->X() + 32 )   >   ( chara1->X() - regionSize ) &&                 //Right2 < Region Left
            ( chara2->X() )                 <   ( chara1->X() + 32 + regionSize ) &&   //Left2 > Region Right
            ( chara2->Y() + 32)   >   ( chara1->Y() - regionSize ) &&                 //Bottom2 < Region Top
            ( chara2->Y() )                 <   ( chara1->Y() + 32 + regionSize ) )    //Top2 > Region Bottom
    {
        return true;
    }
    return false;
}

bool NpcManager::FacingNpc( Character *chara1, Character *chara2 )
{
    if ( chara1->Dir() == LEFT )
    {
        if ( chara2->X() < chara1->X() )
            return true;
    }
    else if ( chara1->Dir() == RIGHT )
    {
        if ( chara2->X() > chara1->X() )
            return true;
    }
    else if ( chara1->Dir() == UP )
    {
        if ( chara2->Y() < chara1->Y() )
            return true;
    }
    else if ( chara1->Dir() == DOWN )
    {
        if ( chara2->Y() > chara1->Y() )
            return true;
    }
    return false;
}
