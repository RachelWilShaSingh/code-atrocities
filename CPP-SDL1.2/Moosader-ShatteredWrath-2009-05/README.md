Rachel J. Morris [Moosader.com](http://www.moosader.com/)

## ABOUT
This is an RPG that a group of us did in Software Engineering at UMKC.

http://www.moosader.com/projects/games/shattered-wrath

## CREDITS

*Rachel J. Morris* Engine programming, game art

*Tim Read* Script programming

*Bret Noble* Design, writing

*Kedar Sanjel* Testing

*Vashanti Raymond* Additional assets, testing
