#pragma once
#include "SDL/SDL.h"
#include "Text.h"
#include "Dialog.h"
#include "Character.h"
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

enum MenuType { MAIN = 0, STATUS = 1, INVENTORY = 2, EQUIPMENT = 3, EXIT = 4 };

struct Button
{
    string text;
    int goToIndex;
    bool active;
    int x, y, textSize;
    int specialFunction;
    int r, g, b;
};

struct Label
{
    string text;
    bool active;
    int x, y, textSize;
    int r, g, b;
};

class Menu : public Dialog
{
    private:
        Button button[10];
        Label label[10];
        int index;
        int goToPrev;
        SDL_Rect cursorCoord;

    public:
        Menu();
        Menu( MenuType type );
        Menu( int index );
        void Setup( int index );
        ~Menu();
        int Index() { return index; }
        void Index( int val ) { index = val; }
        void LoadConfig( MenuType type );

        void Draw( SDL_Surface *destination, SDL_Surface *menugfx );
};







