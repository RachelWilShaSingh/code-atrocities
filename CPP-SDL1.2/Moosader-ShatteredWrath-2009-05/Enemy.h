#pragma once
#pragma once
#include "SDL/SDL.h"
#include "BasicObject.h"
#include <iostream>
#include <string>
#include "Character.h"
#include "Text.h"
using namespace std;

struct StartCoordinates
{
    int x, y, mapId;

    void Setup( int x, int y, int mapId )
    {
        this->x = x;
        this->y = y;
        this->mapId = mapId;
    }
};

class Enemy : public Character
{
    private:
        bool moving, active;
        int moveCounter;
        string type;
        string uniqueId;
        int image, index;
    public:
        Enemy();
        ~Enemy();
        StartCoordinates startCoordinates;
        Direction Update( LuaManager* );
        void LoadMove( LuaManager* );
        void Move();
        void Setup( int index, LuaManager* );
        string GetDialog( LuaManager* );
        string Type() { return type; }
        void Type( string val ) { type = val; }
        int Image() { return image; }
        void Image( int val ) { image = val; }
        void DrawNpc( SDL_Surface *destination, SDL_Surface *source, int offsetX, int offsetY, int currentLevel );
};



