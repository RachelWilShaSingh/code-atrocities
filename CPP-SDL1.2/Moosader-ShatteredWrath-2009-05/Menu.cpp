#include "Menu.h"

Menu::Menu()
{
}

Menu::Menu( MenuType type )
{
    LoadConfig( type );
}

Menu::Menu( int index )
{
    Setup( index );
}

void Menu::Setup( int index )
{
    MenuType type;
    if ( index == MAIN )
        type = MAIN;
    else if ( index == STATUS )
        type = STATUS;
    else if ( index == INVENTORY )
        type = INVENTORY;
    else if ( index == EQUIPMENT )
        type = EQUIPMENT;
    else if ( index == EXIT )
        type = EXIT;
    LoadConfig( type );
}


Menu::~Menu()
{
}

void Menu::Draw( SDL_Surface *destination, SDL_Surface *menugfx )
{
    cout<<"Draw Menu"<<endl;
    // Draw gradient (part of dialog class)
    DrawGradient( destination, menugfx );
    // Draw border (part of dialog class)
    DrawBorder( destination, menugfx );

    // Draw text
    for ( int i=0; i<10; i++ )
    {
        if ( button[i].active )
            DrawText( destination, button[i].text, button[i].x, button[i].y,
                0, 0, button[i].textSize, button[i].r, button[i].g, button[i].b );
        if ( label[i].active )
            DrawText( destination, label[i].text, label[i].x, label[i].y,
                0, 0, label[i].textSize, label[i].r, label[i].g, label[i].b );
    }

    //Draw cursor
    SDL_Rect clipCoord;
    clipCoord.x = 28;
    clipCoord.y = 0;
    clipCoord.w = 23;
    clipCoord.h = 25;
    SDL_BlitSurface( menugfx, &clipCoord, destination, &cursorCoord );
}

void Menu::LoadConfig( MenuType type )
{
    cursorCoord.x = 64;
    cursorCoord.y = 20;

    string filename, temp;
    int index, iTemp;
    ifstream infile;
    bool loadButton = false;
    if ( type == MAIN ) { filename = "menuMain.ass"; }
    else if ( type == STATUS ) { filename = "menuStatus.ass"; }
    else if ( type == INVENTORY ) { filename = "menuInventory.ass"; }
    else if ( type == EQUIPMENT ) { filename = "menuEquipment.ass"; }
    else if ( type == EXIT ) { filename = "menuExit.ass"; }
    infile.open( filename.c_str() );

    // Parse menu file and store info

    while ( infile>>temp )
    {
        if ( temp == "Button" )
        {
            infile>>index;
            loadButton = true;
        }
        else if ( temp == "Label" )
        {
            infile>>index;
            loadButton = false;
        }
        else if ( temp == "X" )
        {
            infile>>iTemp;
            if ( loadButton ) { button[index].x = iTemp; }
            else { label[index].x = iTemp; }
        }
        else if ( temp == "Y" )
        {
            infile>>iTemp;
            if ( loadButton ) { button[index].y = iTemp; }
            else { label[index].y = iTemp; }
        }
        else if ( temp == "Size" )
        {
            infile>>iTemp;
            if ( loadButton ) { button[index].textSize = iTemp; }
            else { label[index].textSize = iTemp; }
        }
        else if ( temp == "Active" )
        {
            if ( loadButton ) { button[index].active = true; }
            else { label[index].active = true; }
        }
        else if ( temp == "NotActive" )
        {
            if ( loadButton ) { button[index].active = false; }
            else { label[index].active = false; }
        }
        else if ( temp == "Goto" )
        {
            infile>>temp;
            if ( temp == "Inventory" ) { button[index].goToIndex = INVENTORY; }
            else if ( temp == "Status" ) { button[index].goToIndex = STATUS; }
            else if ( temp == "Equipment" ) { button[index].goToIndex = EQUIPMENT; }
            else if ( temp == "Quit" ) { button[index].goToIndex = EXIT; }
            else if ( temp == "Inventory" ) { button[index].goToIndex = MAIN; }
            else if ( temp == "None" ) { button[index].goToIndex = -1; }
        }
        else if ( temp == "txtR" )
        {
            infile>>iTemp;
            if ( loadButton ) { button[index].r = iTemp; }
            else { label[index].r = iTemp; }
        }
        else if ( temp == "txtG" )
        {
            infile>>iTemp;
            if ( loadButton ) { button[index].g = iTemp; }
            else { label[index].g = iTemp; }
        }
        else if ( temp == "txtB" )
        {
            infile>>iTemp;
            if ( loadButton ) { button[index].b = iTemp; }
            else { label[index].b = iTemp; }
        }
    }

    infile.close();
}
