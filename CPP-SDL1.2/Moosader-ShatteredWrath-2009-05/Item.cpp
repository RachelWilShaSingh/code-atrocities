#include "Item.h"

Item::Item()
{
    quantity = 0;
    name = "";
    type = "";
}

void Item::Setup( int quantity, string name, string type, bool invisible )
{
    this->quantity = quantity;
    this->name = name;
    this->type = type;
    this->invisible = invisible;
}

void Item::Setup( string name, string type, bool invisible )
{
    Setup( 0, name, type, invisible );
}


