#pragma once
#include "SDL/SDL.h"
#include "Character.h"
#include "Enemy.h"
#include "Level.h"
#include "ImageManager.h"
#include "LuaManager.h"
#include <string>
using namespace std;

const int NPCAMT = 11;

class NpcManager
{
    private:
        Enemy npc[NPCAMT];
    public:
        NpcManager( LuaManager *luaScript );
        ~NpcManager();
        void DrawNpcs( SDL_Surface *destination, ImageManager *img, float offsetX, float offsetY, int currentLevel );
        void Update( Level *level, LuaManager *luaScript );
        int npcCount;
        string TryInteraction( Character *player, LuaManager *luaScript );
        bool Nearby( Character *chara1, Character *chara2 );
        bool FacingNpc( Character *chara1, Character *chara2 );
};


