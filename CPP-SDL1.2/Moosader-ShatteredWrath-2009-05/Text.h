#pragma once

#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"
#include <string>
#include <iostream>
using namespace std;

#define BANK 0
#define BANK_ROOF 1
#define BANK_VAULT 2
#define CATACOMB 3
#define CHURCH 4
#define FAC_BASE 5
#define FAC_MAIN 6
#define FAC_OFFICE 7
#define HOUSE 8
#define PUB 9
#define SLUM 10

void DrawText( SDL_Surface *destination, string message, float x, float y, float offsetX, float offsetY, int size, SDL_Color color );

void DrawText( SDL_Surface *destination, string message, float x, float y, float offsetX, float offsetY, int size, int r, int g, int b );

void DebugOutput( string message );

void DebugOutput( string message, int index );

void DebugOutput( string type, string value );

