#include "MenuManager.h"

MenuManager::MenuManager()
{
    currentMenu = 0;
    active = false;
    Setup();
}

void MenuManager::Draw( SDL_Surface *destination, ImageManager *img )
{
//    menu[currentMenu].ShowDialog( true );
//    menu[currentMenu].Draw( destination, img->menugfx );
}

void MenuManager::Setup()
{
    for ( int i=0; i<MENUAMT; i++ )
    {
        menu[i].Setup( i );
    }
}

void MenuManager::Draw( SDL_Surface *destination, SDL_Surface *menugfx )
{
    menu[currentMenu].Draw( destination, menugfx );
}

