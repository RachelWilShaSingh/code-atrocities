#pragma once
#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"
#include "SDL/SDL_mixer.h"
#include <string>
#include <iostream>
using namespace std;

enum GameState { TITLE = 0, INGAME = 1, MENU = 2, BATTLE = 3 };

class Game
{
	private:
		int screenWidth, screenHeight, screenBPP, maxFPS;
		bool done, fullscreen;
		GameState state;
	public:
        /*
        Everything is drawn to this buffer, and then the buffer is drawn to the screen.
        This is double-buffering, keeps the screen from flashing, makes sure all
        the graphics are ready before drawing it to the screen.
        */
		SDL_Surface *buffer;
		Game();
		~Game();

		int ScreenWidth();
		int ScreenHeight();
		int ScreenBPP();
		int FPS();

		bool Done() { return done; }
		void Done( bool val ) { done = val; }
		void Quit() { done = true; }

		void SetWindowCaption( string caption );
		void ToggleFullscreen();

		void Update();

		GameState State() { return state; }
		void State( GameState val ) { state = val; }
};
