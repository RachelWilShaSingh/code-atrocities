#pragma once
#include "SDL/SDL.h"
#include "SDL/SDL_mixer.h"
#include <iostream>
using namespace std;

class SoundManager
{
    public:
        SoundManager();
        ~SoundManager();
        Mix_Chunk *confirmSound;
        Mix_Chunk *errorSound;
        Mix_Chunk *cursorSound;
        Mix_Chunk *talkSound;
        Mix_Music *song[8];
        Mix_Music *test;
        void StartSong( int id );
};

