#include "Game.h"

Game::Game()
{
    done = false;
    fullscreen = false;
	maxFPS = 60;
	screenWidth = 640;
	screenHeight = 480;
	screenBPP = 32;
	SDL_Init( SDL_INIT_EVERYTHING );
	if ( TTF_Init() == -1 ) { cerr<<"Error initializing SDL fonts"<<endl; }
	if ( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 ) { cerr<<"Error initializing sound"<<endl; }
	SDL_WM_SetCaption( "Shattered Wrath", NULL );
	buffer = NULL;
	state = TITLE;
	if ( fullscreen )
    	buffer = SDL_SetVideoMode( screenWidth, screenHeight, screenBPP, SDL_SWSURFACE | SDL_FULLSCREEN );
    else
        buffer = SDL_SetVideoMode( screenWidth, screenHeight, screenBPP, SDL_SWSURFACE );
	if ( buffer == NULL ) { cerr<<"Error initializing the buffer\n"<<endl; }
}

Game::~Game()
{
	SDL_FreeSurface( buffer );
	Mix_CloseAudio();
    TTF_Quit();
	SDL_Quit();
}

int Game::ScreenWidth() { return screenWidth; }
int Game::ScreenHeight() { return screenHeight; }
int Game::ScreenBPP() { return screenBPP; }
int Game::FPS() { return maxFPS; }

void Game::ToggleFullscreen()
{
    if ( fullscreen )
    {
        fullscreen = false;
        buffer = SDL_SetVideoMode( screenWidth, screenHeight, screenBPP, SDL_SWSURFACE );
    }
    else
    {
        fullscreen = true;
        buffer = SDL_SetVideoMode( screenWidth, screenHeight, screenBPP, SDL_SWSURFACE | SDL_FULLSCREEN );
    }
}

void Game::SetWindowCaption( string caption )
{
    SDL_WM_SetCaption( caption.c_str(), NULL );
}

void Game::Update()
{
	SDL_Flip( buffer );
}
