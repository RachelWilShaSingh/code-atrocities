#include "Dialog.h"

Dialog::Dialog()
{
    visible = false;
    bgCoord.x = 0;
    bgCoord.y = 480-100;
    bgCoord.w = 640;
    bgCoord.h = 100;
    cursorCoord.y = 480-32;
    cursorCoord.x = 640-32;
    cursorCoord.w = 32;
    cursorCoord.h = 32;
}



Dialog::~Dialog()
{
}

void Dialog::DrawGradient( SDL_Surface *destination, SDL_Surface *menugfx )
{
    /*
    Draw background gradient of window
    */
    SDL_Rect coordinates;
    coordinates = bgCoord;
    coordinates.h = (bgCoord.h/20);
    int r, g, b, inc;
    r = g = b = 0x33;
    inc = 0x01;
    //Draw background of window
    for ( coordinates.y = bgCoord.y; coordinates.y < bgCoord.y+bgCoord.h; coordinates.y += coordinates.h )
    {
        r -= inc;
        g -= inc;
        g -= inc;
        SDL_FillRect( destination, &coordinates, SDL_MapRGB( destination->format, r, g, b ) );
    }
}

void Dialog::DrawBorder( SDL_Surface *destination, SDL_Surface *menugfx )
{
    /*
    Draw borders of window
    */
    SDL_Rect coordinates;
    SDL_Rect clipCoord;

    //Vertical sides
    coordinates.w = 1;
    coordinates.h = bgCoord.h;
    coordinates.x = bgCoord.x;
    coordinates.y = bgCoord.y+14;
    SDL_FillRect( destination, &coordinates, SDL_MapRGB( destination->format, 196, 131, 84 ) );
    coordinates.x += 1;
    SDL_FillRect( destination, &coordinates, SDL_MapRGB( destination->format, 102, 73, 50 ) );
    coordinates.x += 1;
    SDL_FillRect( destination, &coordinates, SDL_MapRGB( destination->format, 33, 28, 25 ) );

    coordinates.x = bgCoord.x+bgCoord.w-3;
    SDL_FillRect( destination, &coordinates, SDL_MapRGB( destination->format, 196, 131, 84 ) );
    coordinates.x += 1;
    SDL_FillRect( destination, &coordinates, SDL_MapRGB( destination->format, 102, 73, 50 ) );
    coordinates.x += 1;
    SDL_FillRect( destination, &coordinates, SDL_MapRGB( destination->format, 33, 28, 25 ) );

    //Horizontal sides
    coordinates.w = bgCoord.w;
    coordinates.h = 1;
    coordinates.x = bgCoord.x+14;
    coordinates.y = bgCoord.y;
    SDL_FillRect( destination, &coordinates, SDL_MapRGB( destination->format, 196, 131, 84 ) );
    coordinates.y += 1;
    SDL_FillRect( destination, &coordinates, SDL_MapRGB( destination->format, 102, 73, 50 ) );
    coordinates.y += 1;
    SDL_FillRect( destination, &coordinates, SDL_MapRGB( destination->format, 33, 28, 25 ) );

    coordinates.y = bgCoord.y+bgCoord.h-3;
    SDL_FillRect( destination, &coordinates, SDL_MapRGB( destination->format, 196, 131, 84 ) );
    coordinates.y += 1;
    SDL_FillRect( destination, &coordinates, SDL_MapRGB( destination->format, 102, 73, 50 ) );
    coordinates.y += 1;
    SDL_FillRect( destination, &coordinates, SDL_MapRGB( destination->format, 33, 28, 25 ) );

    //Draw border
    clipCoord.w = 14;
    clipCoord.h = 14;
    coordinates.w = 14;
    coordinates.h = 14;

    // Top-Left
    clipCoord.x = 0;
    clipCoord.y = 0;
    coordinates.x = bgCoord.x;
    coordinates.y = bgCoord.y;
    SDL_BlitSurface( menugfx, &clipCoord, destination, &coordinates );

    // Top-Right
    clipCoord.x = 14;
    clipCoord.y = 0;
    coordinates.x = bgCoord.x+bgCoord.w-14;
    coordinates.y = bgCoord.y;
    SDL_BlitSurface( menugfx, &clipCoord, destination, &coordinates );

    // Bottom-Left
    clipCoord.x = 0;
    clipCoord.y = 14;
    coordinates.x = 0;
    coordinates.y = bgCoord.y+bgCoord.h-14;
    SDL_BlitSurface( menugfx, &clipCoord, destination, &coordinates );

    // Bottom-Right
    clipCoord.x = 14;
    clipCoord.y = 14;
    coordinates.x = bgCoord.x+bgCoord.w-14;
    coordinates.y = bgCoord.y+bgCoord.h-14;
    SDL_BlitSurface( menugfx, &clipCoord, destination, &coordinates );
}

void Dialog::Draw( SDL_Surface *destination, SDL_Surface *menugfx )
{
    /*
    Draw dialog box
    */
    if ( visible )
    {
        //Draw window
        DrawGradient( destination, menugfx );
        DrawBorder( destination, menugfx );

        //Draw text
        DrawMessage( destination );
    }
}


void Dialog::SetCurrentDialog( string val )
{
    /*
    Set what is in the dialog box currently
    */
    message = val;
}

void Dialog::DrawMessage( SDL_Surface *destination )
{
    /*
    Separate the message onto separate lines before drawing
    */
    SDL_Color col = {255, 255, 255};
    int y = 390;
    int x = 8;

    int i=0, j=0;
    int cutoffSize = 60;

    string val = message;
    string temp = "";
    while ( val.size() > cutoffSize )
    {
        //go to next space
        j = cutoffSize;
        while ( val[j] != ' ' )
        {
            j++;
        }

        temp = val.substr( 0, j ).c_str();
        val.erase( 0, j );

        DrawText( destination, temp.c_str(), x, y + (i*22), 0, 0, 18, col );

        i++;
    }

    temp = val.substr( 0, val.size() );
    DrawText( destination, temp.c_str(), x, y + (i*22), 0, 0, 18, col );
}






