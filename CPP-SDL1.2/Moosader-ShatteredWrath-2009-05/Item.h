#pragma once
#include "SDL/SDL.h"
#include <string>
#include "LuaManager.h"
#include "Text.h"
#include "Menu.h"
using namespace std;

class Item
{
    private:
        string name;
        int quantity;
        string type;
        bool invisible;
    public:
        Item();
        void Setup( int quantity, string name, string type, bool invisible );
        void Setup( string name, string type, bool invisible );
        void AddItem() { quantity++; }
        void SubItem() { quantity--; if ( quantity < 0 ) { quantity = 0; } }
        string Name() { return name; }
        void Name( string val ) { name = val; }
        string Type() { return type; }
        void Type( string val ) { type = val; }
        bool Invisible() { return invisible; }
        void Invisible( bool val ) { invisible = val; }
};


