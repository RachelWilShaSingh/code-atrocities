#pragma once
#include "passengerClass.h"

class rjm_carClass
{
    private:
        rjm_passengerClass onboard_passenger[25];
    public:
        void board_passenger( rjm_passengerClass passenger );
};

class rjm_trainClass
{
    private:
        rjm_carClass car[12];
    public:
};

void rjm_carClass::board_passenger( rjm_passengerClass passenger )
{
    passenger.set_on_train( true );
    onboard_passenger = passenger;
}
