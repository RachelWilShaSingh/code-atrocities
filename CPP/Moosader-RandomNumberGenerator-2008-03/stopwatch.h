/*******************************************************
File name:      Stopwatch.h
Author:     Bob Cotter
Date:       8/99
Modified by: Judy Mullins
Description:  This file describes the Stopwatch class that
can be used in general object oriented programming to time
events within programs
*********************************************************/
#ifndef Stopwatch_H
#define Stopwatch_H

#include <iostream>
#include <string>
using namespace std;
#include <time.h>
#include <stdlib.h>

// Definition for Stopwatch
class Stopwatch
{
public:

    Stopwatch();
    //creates a Stopwatch object and sets default values for init_time and end_`
    //sets status to OFF

    // modification member functions
    void start();
    //returns the initial clock time and sets status to ON

    void stop();
    //returns the ending clock time and sets status to OFF

    void reset();
    //sets the initial clock time and ending clock time to 0; sets status to OFF

    // non-member functions
    friend ostream& operator <<(ostream& outs, const Stopwatch& watch);
    //outputs the elapsed time of the Stopwatch object

    // constant member functions
    double resolution(void) const;
    //returns the clock resolution in seconds

    double user(void) const;
	//returns CPU time in seconds

    int status(void) const;
    //returns ON if the Stopwatch is running and OFF otherwise

    // accessor functions
    double get_init_time();
    //returns the beginning time of the Stopwatch

    double get_end_time();
    //returns the ending time of the Stopwatch

private:
    double init_time;
    double end_time;
    int _status;
};


#endif