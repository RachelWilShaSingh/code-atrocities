/****************************************************
File Name:  Stopwatch.cpp
Author:     Bob Cotter
Date:       8/99
Modified by: Judy Mullins
Description:    Source code for Stopwatch class.
****************************************************/

#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
using namespace std;

#include "Stopwatch.h"

#define ON  1
#define OFF 0

// constructor for Stopwatch
Stopwatch::Stopwatch()
{
    init_time = 0;
    end_time = 0;
    _status = OFF;
}

// modification member functions
void Stopwatch::start()
{
    init_time = (double) clock();
    _status = ON;
}

void Stopwatch::stop()
{
    end_time = (double) clock();
    _status = OFF;
}

void Stopwatch::reset()
{
    init_time = 0;
    end_time = 0;
    _status = OFF;
}

// non-member functions
ostream& operator <<(ostream& outs, Stopwatch& watch)
{
    if (watch.get_end_time() != 0)
        outs << (watch.get_end_time() - watch.get_init_time())/CLOCKS_PER_SEC;
    else
        outs << "oops!";
    return outs;
}

// constant member functions
double Stopwatch::resolution() const
{
    return (CLOCKS_PER_SEC);
}

double Stopwatch::user(void) const
{
    return ((end_time - init_time)/(double)CLOCKS_PER_SEC);
}

int    Stopwatch::status() const
{
    return (_status);
}

double Stopwatch::get_init_time()
{
    return init_time;
}

double Stopwatch::get_end_time()
{
    return end_time;
}