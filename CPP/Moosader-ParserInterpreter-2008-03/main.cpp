// Rachel J. Morris
// CS 352 Project 2
// Due March 20th

#include <iostream>
#include <string>
#include "Stack.h"
#include "ExpressionTree.h"
#include "LinkedList.h"
#include "SymbolTable.h"
#include "AssignmentConverter.h"
#include "Parser.h"
using namespace std;

int main()
{
	Parser parser;
	AssignmentConverter converter;
	//listTemplate < SymbolTable > symbolTable;	//the symbol table
	//I changed it, because I felt like it.
	SymbolTable symbolTable;
	int amtOfLines = parser.ReadFile("input.txt");
	ExpressionTree expTree(amtOfLines);
	ofstream outfile;
	outfile.open("output.txt");

    //parse the input file
	parser.Parse(&expTree, &symbolTable);
	//convert stored infix commands to postfix
	converter.convertToPostfix(&expTree);

    //print expression tree
	expTree.printPostfix(cout);
	outfile<<"postfix expressions after conversion:"<<endl;
	expTree.printPostfix(outfile);

	//evaluate postfix
	expTree.evaluate(&symbolTable);

	//print results
	expTree.printPostfix(cout);
	outfile<<"\n\npostfix expressions after traversal:"<<endl;
	expTree.printPostfix(outfile);
	cout<<endl<<endl;
	symbolTable.print(cout);

	outfile<<endl<<endl;
	symbolTable.print(outfile);

	outfile.close();
	system("pause");
	return 0;
}