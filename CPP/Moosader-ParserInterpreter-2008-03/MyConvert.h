// from http://www.parashift.com/c++-faq-lite/misc-technical-issues.html#faq-39.1
// because I am using atof for string to float/double.
#include <iostream>
#include <sstream>
#include <string>
#include <stdexcept>

class BadConversion : public std::runtime_error {
	public:
	BadConversion(const std::string& s)
		: std::runtime_error(s)
		{ }
};

inline std::string stringify(double x)
{
	std::ostringstream o;
	if (!(o << x))
		throw BadConversion("stringify(double)");
	return o.str();
}