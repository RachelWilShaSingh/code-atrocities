#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include "Stack.h"
#include "function.h"
#include "MyConvert.h"
#include "SymbolTable.h"
#include "LinkedList.h"
using namespace std;

class ExpressionTree
{
	public:
		string *infixArray;			//an array of infix equations
		string *postfixString;		//each line is the postfix equivalent of the equation in the infixArray of corresponding index 
		string *answer;				//an array of answers
		StackClass *postfixArray;   //an array of stacks...
		int maxSize;				//this holds the amount of ';' that were in the input file, and creates dynamic arrays based on this size
		int infixIndex;				
		int postfixIndex;			
		int postfixIndexB;			
		// each line of code will be one index in the array,
		// each entry in the stack is one character in the expression tree.

		ExpressionTree(int size)
		{
			//create dynamic arrays
			infixArray = new string[size];
			postfixArray = new StackClass[size];
			postfixString = new string[size];
			answer = new string[size];
			maxSize = size;
			infixIndex = postfixIndex = postfixIndexB = 0;
			for (int i=0; i<size; i++)
			{
			    infixArray[i].clear();
			}
		}
		void setup(int size)
		{
			//create dynamic arrays
		    infixArray = new string[size];
			postfixArray = new StackClass[size];
			postfixString = new string[size];
			answer = new string[size];
			maxSize = size;
			infixIndex = postfixIndex = postfixIndexB = 0;
		}
		~ExpressionTree()
		{
		    if ( infixIndex != 0 )      //make sure it isn't empty
                delete [] infixArray;
		    if ( postfixIndex != 0 )
                delete [] postfixArray;
            if ( postfixIndexB != 0 )
                delete [] postfixString;
			if ( answer != 0 )
				delete [] answer;
		}
		void addToInfix(char value)
		{
		    infixArray[infixIndex] += value;
		}
		void nextInfix()
		{
		    if ( infixIndex + 1 < maxSize )
                infixIndex++;
		}
		void printInfix(ostream &out)
		{
		    for ( int i=0; i<infixIndex; i++ )
		    {
		        if ( infixArray[i] != "" )
                    cout<<infixArray[i]<<endl;
		    }
		}
		void printPostfix(ostream &out)
		{
		    for (int i=0; i<maxSize; i++)
		    {
				if ( postfixString[i] != "" )
				{
					out<<i<<":\t"<<postfixString[i]<<endl;
				}//if ( postfixString[i] != "" )
		    }//for (int i=0; i<maxSize; i++)
		}//void printPostfix(ostream &out)

		void evaluate( SymbolTable *symbolTable )
		{
			cout<<"\n\t-----Evaluate expressions-----\n";
			for (int i=0; i<maxSize; i++)			//for each equation
			{
				if ( postfixString[i] != "" )		//if the line isn't empty
				{
					char tempName = postfixString[i][0];
					for (int j=0; j<postfixString[i].length(); j++)		//check each character
					{	
						if ( postfixString[i][j] == '[' )		//current character is number (numbers are surrounded with [ and ])
						{
							int k=j;
							string tempNum1;
							string tempNum2;
							float tempAns = NULL;
							k++;
							while ( isNumber(postfixString[i][k]) || postfixString[i][k] == '.' )	//if the next character is a number or '.' then
							{																		//it belongs with the current character
								tempNum1 += postfixString[i][k];
								k++;
							}
							k++;	//check next character
							if ( postfixString[i][k] == '[' )	//it's another constant number - we might combine them together if the next one is an operator!
							{
								k++;
								while ( isNumber(postfixString[i][k]) || postfixString[i][k] == '.' )	//similar to above.
								{
									tempNum2 += postfixString[i][k];
									k++;
								}
								k++;
								//get operator
								bool converted = true;
								if ( postfixString[i][k] == '+' )	// get the answer of the two numbers and the operation.
								{
									tempAns = (float)atof(tempNum1.c_str()) + (float)atof(tempNum2.c_str());
								}
								else if ( postfixString[i][k] == '-' )
								{
									tempAns = (float)atof(tempNum1.c_str()) - (float)atof(tempNum2.c_str());
								}
								else if ( postfixString[i][k] == '*' )
								{
									tempAns = (float)atof(tempNum1.c_str()) * (float)atof(tempNum2.c_str());
								}
								else if ( postfixString[i][k] == '/' && (float)atof(tempNum2.c_str()) != 0 )
								{
									tempAns = (float)atof(tempNum1.c_str()) / (float)atof(tempNum2.c_str());
								}
								else if ( postfixString[i][k] == '^' )
								{
									tempAns = pow ( atof (tempNum1.c_str()), atoi(tempNum2.c_str()) );
								}
								else	//aww, it's not an operator, we can't simplify this portion.
								{
									converted = false;
								}
								
								if ( converted )
								{
									//replace old with new!
									string temp = "";
									string tempB = "[";
									tempB += stringify(tempAns);
									tempB += ']'; 
									for (int a=0; a<j; a++)
									{
										temp += postfixString[i][a];
									}
									temp += tempB;
									k++;
									for (int b=k; b<postfixString[i].length(); b++)
									{
										temp += postfixString[i][b];
									}
									postfixString[i] = temp;
									j = j - (tempNum1.length() + tempNum2.length() - tempB.length()) ;
								}
								else	//wasn't an operation, so just continue on our merry way.
								{	
									j += (k-j);
								}
							}
							else		//next item is not a const, continue on our merry way.
							{
								j += (k-j);	
							}
						}//if ( postfixString[i][j] == '[' )
					}//for (int j=0; j<postfixString[i].length(); j++)		//check each character
					symbolTable->updateValue( tempName, postfixString[i] );
				}//if ( postfixString[i] != "" )		//if the line isn't empty 
			}//for (int i=0; i<maxSize; i++)
		}
};